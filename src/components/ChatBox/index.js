import React, { Component } from "react";
import LogoKWSP from "../../assets/images/logo-kwsp.png";
import BotIcon from "../../assets/images/bot-icon.png";
export class ChatBox extends Component {
	render() {
		return (
			<div className="chat-box">
				<div className="chat-box-header">
					<img src={LogoKWSP} width="30" height="30" alt="logo-kwsp.png" />
					Chat with Zara
					<span className="chat-box-toggle">
						<i className="material-icons">close</i>
					</span>
				</div>
				<div className="chat-box-body">
					<div className="chat-box-overlay" />
					<div className="chat-logs">
						<div id="cm-msg" className="chat-msg user">
							<span className="msg-avatar">
								<img src={BotIcon} alt="bot-icon.png" />
							</span>
							<div className="cm-msg-text">
								Hey there friend, Zara WEB here. Im a bot that can help you
								to get info and access services.
							</div>
						</div>

						<div id="cm-msg" className="chat-msg user">
							<span className="msg-avatar">
								<img src={BotIcon} alt="bot-icon.png" />
							</span>
							<div className="cm-msg-text">Hey! Please tell me your name?</div>
						</div>

						<div id="cm-msg" className="chat-msg self">
							<span className="msg-avatar">
								<img
									src="https://image.crisp.im/avatar/operator/196af8cc-f6ad-4ef7-afd1-c45d5231387c/240/?1483361727745"
									alt="avatar"
								/>
							</span>
							<div className="cm-msg-text"> Johnson </div>
						</div>

						<div id="cm-msg" className="chat-msg user">
							<span className="msg-avatar">
								<img src={BotIcon} alt="bot-icon.png" />
							</span>
							<div className="cm-msg-text">
								Which language would you prefer? / Bahasa yang manakah yang
								anda lebih suka?
							</div>
						</div>

						<div id="cm-msg" className="chat-msg self">
							<span className="msg-avatar">
								<img
									src="https://image.crisp.im/avatar/operator/196af8cc-f6ad-4ef7-afd1-c45d5231387c/240/?1483361727745"
									alt="avatar2"
								/>
							</span>
							<div className="cm-msg-text"> English </div>
						</div>
					</div>
				</div>
				<div className="chat-input">
					<form>
						<input type="text" id="chat-input" placeholder="Send a message..." />
						<button type="submit" className="chat-submit" id="chat-submit">
							<i className="material-icons">send</i>
						</button>
						<button type="submit" className="chat-submit1" id="chat-submit">
							<i className="material-icons">attach_file</i>
						</button>
						<button type="submit" className="chat-submit2" id="chat-submit">
							<i className="material-icons">sentiment_satisfied_alt</i>
						</button>
					</form>
					<div className="pweredby">Powered by Symprio Web Chat</div>
				</div>
			</div>
		);
	}
}

export default ChatBox;
