import React, { Component } from "react";

export class CustomSelect extends Component {
	render() {
		return (
			<select className="form-control" >
				<option disabled  value>
					Option 1
				</option>
				<option value={1}>Option 1</option>
				<option value={2}>Option 2</option>
				<option value={3}>Option 3</option>
				<option value={4}>Option 4</option>
				<option value={5}>Option 5</option>
				<option value={6}>Option 6</option>
				<option value={7}>Option 7</option>
			</select>
		);
	}
}

export default CustomSelect;
