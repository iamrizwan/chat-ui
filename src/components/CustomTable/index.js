import React, { Component } from "react";

export class CustomTable extends Component {
	render() {
		return (
			<div>
				<div>
					<table className="table table-striped">
						<thead className="thead-light">
							<tr>
								<th scope="col">Intent</th>
								<th scope="col">Category</th>
								<th scope="col">Agent</th>
								<th scope="col">Status</th>
								<th scope="col">Ave/day</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">1</th>
								<td>Mark</td>
								<td>Otto</td>
								<td>@mdo</td>
								<td>@mdo</td>
							</tr>
							<tr>
								<th scope="row">2</th>
								<td>Jacob</td>
								<td>Thornton</td>
								<td>@fat</td>
								<td>@fat</td>
							</tr>
							<tr>
								<th scope="row">3</th>
								<td>Larry</td>
								<td>the Bird</td>
								<td>@twitter</td>
								<td>@twitter</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

export default CustomTable;
