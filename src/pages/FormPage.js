import React, { Component } from "react";
import CustomSelect from "../components/CustomSelect";

export class FormPage extends Component {
	render() {
		return (
			<div className="App container">
				<center>
					<h3 className="text-left">
						<i
							className="fas fa-broadcast-tower text-info"
							style={{ margin: 5 }}
						/>
						<strong className="text-info"> Broadcast Message</strong>
					</h3>
					<div className="row">
						<div className="col-sm-4">
							<CustomSelect />
						</div>
						<div className="col-sm-4">
							<CustomSelect />
						</div>
						<div className="col-sm-4">
							<CustomSelect />
						</div>
					</div>
					<br />
					<div className="row">
						<div className="col-sm-9">
							<input
								type="text"
								className="form-control"
								placeholder="Title"
								aria-label="Username"
								aria-describedby="basic-addon1"
							/>
						</div>
						<div className="col-sm-3">
							<input
								type="text"
								className="form-control"
								placeholder="Sub-Title"
								aria-label="Username"
								aria-describedby="basic-addon1"
							/>
						</div>
					</div>
					<br />
					<h5 className="text-info text-left"> New Broadcast</h5>
					<div className="row">
					<div className="col-sm-4">Notification Icon</div>
						<div className="col-sm-4">
							<input
								type="file"
								className="form-control btn-success"
								placeholder="Username"
								aria-label="Username"
								aria-describedby="basic-addon1"
								title="Upload"
								style={{border:'none',outline:'none', textIndent: '-999em'}}
							/>
							<i
								className="fas fa-cloud-upload-alt fa-2x"
								style={{position:'absolute',top:2,right:30,color:'white'}}
							/>
						</div>
						<div className="col-sm-4">
							<input
								type="text"
								className="form-control"
								placeholder="URL"
								aria-label="Username"
								aria-describedby="basic-addon1"
							/>
						</div>
					</div>

					<br />
					<h5 className="text-info text-left">Arrangement</h5>
					<div className="row">
						<div className="col-sm-4">Activity</div>
						<div className="col-sm-4">
							<CustomSelect />
						</div>
						<div className="col-sm-4">
							<input
								type="text"
								className="form-control"
								placeholder="URL"
								aria-label="Username"
								aria-describedby="basic-addon1"
							/>
						</div>
					</div>
					<br />
					<div className="row">
						<div className="col-sm-4">Sentiment</div>
						<div className="col-sm-4">
							<CustomSelect />
						</div>
						<div className="col-sm-4">
							<input
								type="text"
								className="form-control"
								placeholder="URL"
								aria-label="Username"
								aria-describedby="basic-addon1"
							/>
						</div>
					</div>
					<br />
					<div className="row">
						<div className="col-sm-4">Region</div>
						<div className="col-sm-4">
							<CustomSelect />
						</div>
						<div className="col-sm-4">
							<input
								type="text"
								className="form-control"
								placeholder="URL"
								aria-label="Username"
								aria-describedby="basic-addon1"
							/>
						</div>
					</div>
					<br />
					<div className="row">
						<div className="col-sm-4">Intrest</div>
						<div className="col-sm-4">
							<CustomSelect />
						</div>
						<div className="col-sm-4" />
					</div>
					<br />
					<div className="row">
					<div className="col-sm-4"></div>
						<div className="col-sm-4">
							<button type="button" className="btn btn-success btn-block">
								Broadcast
							</button>
						</div>
						<div className="col-sm-4">
							<button type="button" className="btn btn-primary btn-block">
								Cancel
							</button>
						</div>
						<div className="col-sm-4" />
					</div>
				</center>
			</div>
		);
	}
}

export default FormPage;
