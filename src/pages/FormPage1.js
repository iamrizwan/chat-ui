import React, { Component } from "react";
import CustomSelect from "../components/CustomSelect";
import  CustomTable  from "../components/CustomTable";

export class FormPage1 extends Component {
	render() {
		return (
			<div className="App container-fluid">
				<h3 className="text-left">
					<i className="fas fa-broadcast-tower text-info" style={{ margin: 5 }} />
					<strong className="text-info">Knowledge Base</strong>
				</h3>
				<br />
				<div className="row">
					<div className="col-sm-6">
						<div className="row">
							<div className="col-sm-6">
								<div className="input-group mb-3">
									<input
										type="text"
										className="form-control"
										placeholder="Search"
										aria-label="Recipient's username"
										aria-describedby="basic-addon2"
									/>
									<i
										className="fas fa-search"
										style={{
											position: "absolute",
											top: 10,
											right: 20,
											color: "gray"
										}}
									/>
								</div>
							</div>
						</div>
						<br />
						<div className="row">
							<div className="col-sm-12">
								<CustomTable />
							</div>
						</div>
						<br />
					</div>
					<div className="col-sm-6">
						<div className="row">
							<div className="col-sm-6">
								<h5 className="text-dafault">INTENT</h5>
								<CustomSelect />
							</div>
							<div className="col-sm-6">
								<h5 className="text-dafault">ACTIVE</h5>
								<CustomSelect />
							</div>
						</div>
						<br />
						<div className="row">
							<div className="col-sm-6">
								<h5 className="text-dafault">AGENT/BOT</h5>
								<CustomSelect />
							</div>
							<div className="col-sm-6">
								<p className="text-dafault" />
								<h6 className="text-dafault">VOLUME: 178/DAY</h6>
								<h6 className="text-dafault">NLP TARINED: 1,474</h6>
							</div>
						</div>
						<br />
						<div className="row">
							<div className="col-sm-6">
								<button
									type="button"
									className="btn btn-primary"
									style={{ margin: 5 }}
								>
									English
								</button>
								<button
									type="button"
									className="btn btn-default"
									style={{ margin: 5 }}
								>
									Malay
								</button>
							</div>
						</div>
						<br />
						<div className="row">
							<div className="col-sm-3">
								<h5>Description</h5>
							</div>
							<div className="col-sm-9">
								<input
									type="text"
									className="form-control"
									placeholder="URL"
									aria-label="Username"
									aria-describedby="basic-addon1"
								/>
							</div>
						</div>
						<br />
						<div className="row">
							<div className="col-sm-12">
								<button
									type="button"
									className="btn btn-default"
									style={{ margin: 5 }}
								>
									Answer
								</button>
								<button
									type="button"
									className="btn btn-default"
									style={{ margin: 5 }}
								>
									Guided
								</button>
								<button
									type="button"
									className="btn btn-default"
									style={{ margin: 5 }}
								>
									Cards
								</button>
								<button
									type="button"
									className="btn btn-default"
									style={{ margin: 5 }}
								>
									Chat Form
								</button>
								<button
									type="button"
									className="btn btn-default"
									style={{ margin: 5 }}
								>
									RSS
								</button>
							</div>
						</div>
						<br />
						<div className="row">
							<div className="col-sm-10">
								<textarea
									className="form-control"
									aria-label="With textarea"
									defaultValue={""}
								/>
								<h6 className="text-left btn-left">Pause: 4 Seconds</h6>
							</div>
							<div className="col-sm-2">
								<i
									className="fas fa-trash"
									style={{
										position: "absolute",
										top: 30,
										right: 60,
										color: "black"
									}}
								/>
							</div>
						</div>
						<br />
						<div className="row">
							<div className="col-sm-10">
								<input
									type="text"
									className="form-control"
									placeholder="lorem lorem lorem lorem "
									aria-label="Username"
									aria-describedby="basic-addon1"
								/>
							</div>
							<div className="col-sm-2">
								<i
									className="fas fa-plus"
									style={{
										position: "absolute",
										top: 10,
										right: 60,
										color: "black"
									}}
								/>
							</div>
						</div>
						<br />
					</div>
				</div>
			</div>
		);
	}
}

export default FormPage1;
