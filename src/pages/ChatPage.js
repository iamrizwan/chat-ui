import React, { Component } from "react";
import "../App.css";
import $ from "jquery";
import ChatBox from "../components/ChatBox";

class ChatPage extends Component {
	componentDidMount = () => {
		$("#chat-circle").click(function() {
			$("#chat-circle").toggle("scale");
			$(".chat-box").toggle("scale");
		});
		$(".chat-box-toggle").click(function() {
			$("#chat-circle").toggle("scale");
			$(".chat-box").toggle("scale");
		});
	};

	render() {
		return (
			<div className="ChatPage">
				<div id="center-text">
					<h2>Symprios webchat </h2>
					<p>AI EnableduWebchat from Symprio</p>
				</div>
				<div id="body">
					<div id="chat-circle" className="btn btn-raised">
						<div id="chat-overlay" />
						<i className="material-icons">chat</i>
					</div>
					<ChatBox />
				</div>
			</div>
		);
	}
}

export default ChatPage;
