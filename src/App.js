import React, { Component } from "react";
import "./App.css";
import ChatPage from "./pages/ChatPage";
import FormPage from "./pages/FormPage";
import FormPage1 from "./pages/FormPage1";
import { Route } from "react-router-dom";
class App extends Component {
	render() {
		return (
			<div>
				<Route exact path="/" component={ChatPage} />
				<Route exact path="/form" component={FormPage} />
				<Route exact path="/form1" component={FormPage1} />
			</div>
		);
	}
}

export default App;
